@extends("layouts.base")

@section('header')
<div class="pageheader">
  <h2>{{ $titulo }}</h2>
</div>
@endsection

@section('contenido')

<section>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <div class="row">
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-heading">
                        <div style="padding:20px 0 0 20px">
                            {{-- {{ Form::open(["url" => "tarea/nueva" , "method" => "post", 'role' => "form"]) }} --}}
                            <input type="text" placeholder="Nueva tarea..." name="nueva_tarea" id="nueva_tarea"> 
                            <input type="checkbox" name="categoria_php" id="categoria_php" value="1"><label for="categoria_php">PHP</label>
                            <input type="checkbox" name="categoria_javascript" id="categoria_javascript" value="2"><label for="categoria_javascript">Javascript</label>
                            <input type="checkbox" name="categoria_css" id="categoria_css" value="3"><label for="categoria_css">CSS</label>
                            <input type="button" id="agregar" value="Añadir">
                            {{-- {{ Form::close() }} --}}
                        </div>
                        <div class="card-body">
                            <table id="lista" class="table table-striped table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th>Tarea</th>
                                        <th>Categorias</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="cuerpo">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <br />
        {{-- <script>
            $(document).ready(function(){
                function eliminarTarea(id_tarea){
                     var id = id_tarea.split('_');
                     alert(id)
                    // $.ajax({
                    //     type: 'GET',
                    //     url: 'eliminarTarea',
                    //     header:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    //     data :  {_token : $('meta[name="csrf-token"]').attr('content'), },
                    //     success: function(data) {
                    //         console.log(data)
                            
					// 	},
                    //     error: function(xhr, textStatus, thrownError) {
                    //         console.log(xhr)
                    //     }
                    // })
                }
            })

        </script> --}}
    </section>
    
@endsection
