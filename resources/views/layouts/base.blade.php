<!DOCTYPE html>
<html lang="en">

@include('layouts.header')

<body>

    <section class="content-wrapper">
            
            @yield("header")

        <section>
        
            @yield("contenido")
 
        </section>
    </section>

@include('layouts.footer')

</body>


</html>