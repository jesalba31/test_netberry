<?php

namespace App\Http\Controllers;

use DB;
use \App\Tareas;
use \App\Categorias;
use \App\TareasCategorias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TareaController extends Controller
{
    public function index() {
        $titulo = "Gestor de tareas";
        // $tareas = TareasCategorias::all();
        $tareas = Tareas::all();
        return view('tareas.index', ['titulo' => $titulo, 'tareas' => $tareas]);
    }

    public function nueva_tarea(){
        $nombre_tarea = Input::get("tarea");    
        $cat_php = Input::get("cat_php");    
        $cat_js = Input::get("cat_js");    
        $cat_css = Input::get("cat_css");    

        $tarea = new Tareas();
        $tarea->descripcion = $nombre_tarea;
        $tarea->save();
        
        if ($cat_php !== null) {
            $tarea_categorias = new TareasCategorias();
            $tarea_categorias->tarea_id = $tarea->id;
            $tarea_categorias->categoria_id = $cat_php;
            $tarea_categorias->save();
        }
        if ($cat_js !== null) {
            $tarea_categorias = new TareasCategorias();
            $tarea_categorias->tarea_id = $tarea->id;
            $tarea_categorias->categoria_id = $cat_js;
            $tarea_categorias->save();
        }
        if ($cat_css !== null) {
            $tarea_categorias = new TareasCategorias();
            $tarea_categorias->tarea_id = $tarea->id;
            $tarea_categorias->categoria_id = $cat_css;
            $tarea_categorias->save();
        }

        return "Creado";
    }
    
    public function obtener_datos(){
        $tareas = DB::table('categorias as c')
            ->select('t.id', 't.descripcion AS Tarea', DB::raw("(GROUP_CONCAT(DISTINCT c.descripcion)) as `Categorias`"))
			->leftjoin('tareas_categorias as tc','c.id' , '=', 'tc.categoria_id')
			->leftjoin('tareas as t','t.id' , '=', 'tc.tarea_id')
			->groupBy('t.id')->get();
        $val_tareas = json_decode($tareas);
        return $val_tareas[0]->id == null ? "NOT" : $val_tareas;
    }

    public function eliminar_tarea($id_tarea){
        $tarea = Tareas::find($id_tarea);
        $tarea->delete();
        return "Eliminado";
    }
}
