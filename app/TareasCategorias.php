<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TareasCategorias extends Model
{
    protected $table = "tareas_categorias";
    
    protected $fillable = array("tarea_id", "categoria_id");
    
}
