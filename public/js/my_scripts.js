            $(document).ready(function(){

                eliminarTarea = function(tarea){
                     var id = tarea.split('_');
                    swal({
                        title: "¿Estas seguro?",
                        text: "Al borrar la tarea no podrá deshacer la acción",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Cancelar", "Eliminar"],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("La tarea ha sido eliminado exitosamente", {
                            icon: "success",
                            button: "Continuar",
                          });
                            $.ajax({
                                type: 'POST',
                                url: 'eliminar_tarea/'+id[1],
                                header:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                data : {_token : $('meta[name="csrf-token"]').attr('content'), },
                                success: function(data) {
                                    if (data == "Eliminado") {
                                        obtenerDatos();
                                    }
                                },
                                error: function(xhr, textStatus, thrownError) {
                                    console.log(xhr)
                                }
                            })
                        } else {
                          swal("No ha pasado nada!", {
                          icon: "info",
                          });
            
                        }
                      });
                }
                
                function obtenerDatos(){
                    $.ajax({
                        type: 'GET',
                        url: 'obtener_datos',
                        header:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data : {_token : $('meta[name="csrf-token"]').attr('content'), },
                        success: function(data) {
                            if (data == "NOT") {
                                $("#cuerpo").html("");
                                var tr = `<p style="padding: 10px 0 0 10px"> No hay tareas pendientes </p>`;
                                $("#cuerpo").append(tr)
                            } else {
                                $("#cuerpo").html("");
                                for(var i=0; i<data.length; i++){
                                    var tr = `<tr>
                                    <td id="tarea_`+data[i].id+`">`+data[i].Tarea+`</td>
                                    <td>`+data[i].Categorias+`</td>
                                    <td><span style="color:red;cursor:pointer" class="fas fa-times" onclick="eliminarTarea('tarea_`+data[i].id+`')"></span></td>
                                    </tr>`;
                                    $("#cuerpo").append(tr)
                                }
                            }
						},
                        error: function(xhr, textStatus, thrownError) {
                            console.log(xhr)
                        }
                    })
                }
                obtenerDatos();
                
                $( "#agregar" ).click(function() {
                    var tarea = $("#nueva_tarea").val();
                    var cat_php = $('#categoria_php').prop('checked') == true ? $("#categoria_php").val() : null;
                    var cat_js = $('#categoria_javascript').prop('checked') == true ? $("#categoria_javascript").val() : null;
                    var cat_css = $('#categoria_css').prop('checked') == true ? $("#categoria_css").val() : null;
                    $("#nueva_tarea").text("");
                    $.ajax({
							type: 'POST',
							url: "nueva_tarea", // change as needed
							headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
							data : {'tarea': tarea, 'cat_php': cat_php, 'cat_js': cat_js, 'cat_css': cat_css, _token : $('meta[name="csrf-token"]').attr('content'), },
							success: function(data) {
                                if ( data == "Creado" ) {
                                    swal("La tarea ha sido registrada exitosamente", {
                                        icon: "success",
                                        button: "Continuar",
                                      });
                                    $('#nueva_tarea').val(""); 
                                    $('input:checkbox').removeAttr('checked');
                                    obtenerDatos();
                                } else {
                                    
                                }
                                
							},
							error: function(xhr, textStatus, thrownError) {
								console.log(xhr)
							}
						});	
                });
            })
