<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('tarea')->group(function(){
    Route::get("index" , "TareaController@index");
    Route::post("nueva_tarea" , "TareaController@nueva_tarea");
    Route::get("obtener_datos" , "TareaController@obtener_datos");
    Route::post("eliminar_tarea/{id_tarea}" , "TareaController@eliminar_tarea");

});
